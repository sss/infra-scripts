#!/bin/bash

BASEDIR="${0%/*}"

check_dependencies() {
    # I install the dependencies of this script using this command:
    # cave import \
    #     --description 'Dependencies for the paludis nightlies script' \
    #     --run-dependency app-doc/doxygen \
    #     --run-dependency app-text/tidy \
    #     --run-dependency dev-db/sqlite:3 \
    #     --run-dependency dev-lang/ruby \
    #     --run-dependency dev-libs/boost \
    #     --run-dependency dev-libs/jansson \
    #     --run-dependency dev-python/Pygments \
    #     --run-dependency dev-python/epydoc \
    #     --run-dependency dev-ruby/allison \
    #     --run-dependency dev-ruby/syntax \
    #     --run-dependency mail-client/mutt \
    #     --run-dependency sys-devel/gcc:4.8 \
    #     --run-dependency sys-devel/gcc:4.9 \
    #     unpackaged/nightlies 1 \
    #     -l /var/empty -x -- \
    #     --favour w3m
    #
    # python bindings: boost
    # ruby bindings: ruby
    # docs: doxygen tidy
    #     python: epydoc pygments
    #     ruby: allison syntax
    # send nightlies mail: mutt
    # check binaries
    local x
    for x in python ruby doxygen tidy epydoc pygmentize allison mutt; do
        [[ -x $(type -P ${x}) ]] || die "${x} not found in PATH"
    done
    # check gems (syntax)
    # check libraries (boost)
}

settings() {
    local f settings_functions
    settings_functions=$(declare -F | sed -n -e 's/^declare\s\+-f\s\+settings_//p')
    for f in ${settings_functions}; do
        settings_${f}
    done
}

settings_build() {
    BASECFLAGS="-march=k8 -pipe -O2 -D__ZLIN_WAS_HERE"

    BUILDDIR="${BASEDIR}/paludis-HEAD"
    PATCH="${BASEDIR}/fix.patch"

    PID="$$"
    LOG="${BASEDIR}/nightly-out"

    RECIPIENT="${BASEDIR}/Recipient"
    LAST_SHA1SUM="${BASEDIR}/Last_sha1sum"

    GCCS="4.8 4.9"

    export CHOST="x86_64-pc-linux-gnu"
    export CFLAGS="${BASECFLAGS}"
    export CXXFLAGS="${CFLAGS}"
    export MAKEOPTS="-j9 -l9"

    [[ -d ${HOME}/bin ]] || mkdir -p "${HOME}"/bin
    # ldconfig is in /sbin/
    export PATH="${HOME}/bin:${PATH}:/sbin"
}

settings_git() {
    URL="git://git.exherbo.org/paludis/paludis.git"
    #BRANCHES="0.60 master"
    BRANCHES="master"
    export GIT_DIR="${HOME}/paludis.git"
}

settings_mail() {
    export LANG="en_GB.utf8"

    FROM="Bo Ørsted Andresen <bo.andresen@zlin.dk>"
    TO="paludis-dev@lists.exherbo.org"

    # mutt uses $EMAIL. Use UTF-8 for the email.
    export EMAIL="${FROM}"
}

die() {
    eerror "$@"
    exit 1
}

eerror() {
    echo "$@" >&2
    return 1
}

edo() {
    echo "$@" >&2
    "$@" || die "$@ failed"
}

checkvars() {
    local v
    for v in "$@"; do
        [[ -z "${!v}" ]] && die "\$$v unset or empty"
    done
}

cleanold() {
    if [[ -d "${BUILDDIR}${SUFFIX}" ]]; then
        # rm -rf fails when the target does not have write permissions for all files
        chmod -R u+w "${BUILDDIR}${SUFFIX}" || die "chmod failed"
        edo rm -rf "${BUILDDIR}${SUFFIX}"/ ||
	    eerror "rm -rf ${BUILDDIR}${SUFFIX} failed" || return 1
    fi
    return 0
}

gitfetch() {
    if [[ ! -e ${GIT_DIR} ]]; then
        git clone --bare "${URL}" "${GIT_DIR}" || die "git clone --bare ${URL} ${GIT_DIR} failed"
        git config remote.origin.url "${URL}" || die "git config remote.origin.url ${URL} failed"
    fi
    local b
    for b in ${BRANCHES}; do
        git fetch -f origin ${b}:${b} || die "git fetch -f origin ${b}:${b} failed"
    done
    sha1sum=$(git describe 2>/dev/null)
    [[ -z ${sha1sum} ]] && sha1sum=$(git rev-parse HEAD)
}

gitlog() {
    git log 'HEAD^!'
}

abort_if_nothing_changed() {
    local old_sha1sum
    [[ -e ${LAST_SHA1SUM} ]] && old_sha1sum=$(<"${LAST_SHA1SUM}")
    if [[ -n ${sha1sum} && ${sha1sum} == ${old_sha1sum} && ! -f ${PATCH} ]]; then
        eerror "Aborting at sha1sum '${sha1sum:0:8}' since nothing has changed since last run."
        echo "Remove ${LAST_SHA1SUM} or create ${PATCH} to continue anyway." >&2
	exit 0
    fi
    echo "${sha1sum}" > "${LAST_SHA1SUM}"
}

distcheck() {
    if [[ -f ${PATCH} ]]; then
        patch -p1 < "${PATCH}" || eerror "patch failed" || return 1
    fi
    [[ -n ${sha1sum} ]] || eerror "empty sha1sum" || return 1
    ${CC:-gcc} -v
    gcc=$(${CC:-gcc} -dumpversion)
    checkvars gcc
    ./autogen.bash || eerror "autogen.bash failed" || return 1
    # the ./configure options are really irrelevant for make distcheck but the
    # configure step fails without some of them
    ./configure \
        --with-git-head="${sha1sum}" \
	--with-repositories=default,accounts \
	--with-clients=default,cave \
        --with-environments=default,portage \
	--with-default-distribution=exherbo \
	--with-config-framework=eclectic || eerror "configure failed" || return 1
    make ${MAKEOPTS} distcheck || eerror "distcheck failed" || return 1
}

body() {
    local logs processes= gcc branch
    for ((i=0;i<${#RESULTS[*]};i++)); do
        gcc=${RESULTS[i]%%:*}
        branch=${gcc%_*}
	gcc=${gcc#*_}
        processes+=$'\n'"Process $((i+1)) on GCC ${gcc} at branch ${branch} started at ${DATES[i]%% *}, finished at ${DATES[i]#* } and ended in ${RESULTS[i]#*:}."
    done
    if [[ ${#RESULTS[*]} -gt 1 ]]; then
        logs="logs have"
    else
        logs="log has"
        processes=$'\n'"Process on GCC ${gcc} at branch ${branch} started at ${DATES%% *} and finished at ${DATES#* } and ended in ${RESULTS#*:}."
    fi

    cat <<EOF
The Paludis nightly (${DATE} - ${sha1sum}) build resulted in ${RESULT:-${result}}. The full
compressed ${logs} been attached.

-------------------------------------------------------------------------------------------

$(<"${LOGS[0]}")

-------------------------------------------------------------------------------------------
${processes}

-------------------------------------------------------------------------------------------

$(<"${LOGS[1]}")
EOF
}

mail() {
    # don't send mails to the list for trivial failures (e.g. git timeout or patch
    # failure) that happened before ${sha1sum} got set
    if [[ -z ${sha1sum} ]]; then
        TO="${FROM}"
    fi
    # read recipient override if available
    if [[ -f ${RECIPIENT} ]]; then
        TO="$(<"${RECIPIENT}")"
    fi

    # send the mail
    body | mutt -s "Paludis nightly (${DATE} - ${sha1sum}) : ${RESULT:-${result}}" -a "${ATTACHMENTS[@]}" -- ${TO}
    [[ -z ${PIPESTATUS[*]//[ 0]} ]] || die "failed to send mail" || return 1
}

run_nightlies() {
    if [[ -x ${CCACHE:=$(type -P ccache)} ]]; then
        for symlink in "${CC:-gcc}" "${CXX:-g++}"; do
            if [[ ! -e ${HOME}/bin/${symlink} || ! -L ${HOME}/bin/${symlink} ]]; then
                rm -f "${HOME}/bin/${symlink}" || die "rm -f ${symlink} failed"
                ln -s "${CCACHE}" "${HOME}/bin/${symlink}" || die "ln -s ${symlink} failed"
            fi
        done
        if [[ ! -d ${HOME}/.ccache_dir${CCSUFFIX} ]]; then
            mkdir -p "${HOME}/.ccache_dir${CCSUFFIX}" || die "mkdir -p failed"
            chmod g+ws "${HOME}/.ccache_dir${CCSUFFIX}" || die "chmod g+ws failed"
        fi
        export CCACHE_DIR="${HOME}/.ccache_dir${CCSUFFIX}"
    fi

    date1=$(date -u +%T) || die "date1 failed"
    pushd "${BUILDDIR}${SUFFIX}"/ || eerror "pushd failed" || return 1
    log="${LOG}${SUFFIX/_/-}-${PID}"
    distcheck &> "${log}" && result=success || result=failure
    if [[ ${result} == failure ]]; then
	pwd >&2
        find . -type f -name '*.epicfail' -or -name '*.log' >&2
        find . -type f -name '*.epicfail' -or -name '*.log' -print0 | xargs -r0 tar cjf "${log}"-epicfail.tar.bz2 
        additional_attachment="${log}"-epicfail.tar.bz2
    fi
    popd || eerror "popd failed"
    date2=$(date -u +%T) || die "date2 failed"
    bzip2 "${log}" || die "bzip2 failed"
    log+=".bz2"
}

results() {
    RESULTS+=("${1}:${2}")
    DATES+=("${3} ${4}")
    shift 4
    ATTACHMENTS+=("${@}")
}

results_to_file() {
    {
      local x
      for x in "${@}"; do
          echo "${x}"
      done
    } > ~/${b}_${g}
}

parallellise_nightlies() {
    for b in ${BRANCHES}; do
        for g in "${@}"; do
            SUFFIX="-${b}_${g}${dirty}"
	    cleanold || return 1
	    mkdir "paludis-HEAD${SUFFIX}/"  || eerror "mkdir paludis-HEAD${SUFFIX} failed" || return 1
	    git --work-tree="paludis-HEAD${SUFFIX}/" checkout -f ${b} || eerror "git checkout ${b} failed" || return 1
	    CC="gcc-${g}" CXX="g++-${g}" CCSUFFIX="_${g}" SUFFIX="-${b}_${g}${dirty}" \
	        run_nightlies ${g} && \
		results_to_file "${b}_${g}" "${result}" "${date1}" "${date2}" "${log}" "${additional_attachment}" &
        done
	wait
    done
}

parse_gccs() {
    while [[ ${#} -gt 0 ]]; do
        parallellise_nightlies ${1} # ${2}
	shift
        # [[ ${#} -gt 0 ]] && shift
    done
}

main() {
    check_dependencies
    settings
    cd "${BASEDIR}"/ || die "cd to ${BASEDIR}/ failed"
    log="${LOG}-${PID}"
    gitfetch &> "${log}-fetch.log"
    gitlog &> "${log}-log.log"
    LOGS=( "${log}-log.log" "${log}-fetch.log" )
    abort_if_nothing_changed

    if [[ -f ${PATCH} ]]; then
        dirty="-dirty"
        sha1sum+="-dirty"
    fi

    DATE=$(date -u +%D) || die "DATE failed"
    ATTACHMENTS=()
    parse_gccs ${GCCS}
    for b in ${BRANCHES}; do
        for g in ${GCCS}; do
            i=1
            while read line; do
                export -n i${i}="${line}"
                ((i++))
            done < ~/${b}_${g}
	    #       gcc/branch result  date1   date2   log/attachment attachment2?
            results "${i1}"    "${i2}" "${i3}" "${i4}" "${i5}"        ${i6}
        done
    done
    RESULT=success
    local res
    for res in "${RESULTS[@]#*:}"; do
        [[ ${res} != success ]] && RESULT=${res}
    done

    # send  the mail and if successful remove the log
    mail
}

main "${@}"

exit 0

