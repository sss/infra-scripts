TARBALL=layman_repositories.tar.bz2
ENVIRONMENT=:playboy-gentoo
PLAYBOY_OPTIONS=(
    # list "portage-stable", "spring" and "gentoo_prefix" respectively
    # in layout.conf masters, which aren't in layman
    --blacklist chromiumos --blacklist GoGooOS --blacklist heroxbd
    # invalid repo_name
    --blacklist gentoo-mate-112
    # syntax error in layout.conf
    --blacklist erayd
)
